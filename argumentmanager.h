/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENTMANAGER_H
#define ARGUMENTMANAGER_H

/**
 * @brief Allow option arguments to be optional
 * @note [POSIX.1-2008 12.2 guideline 7] states that all option arguments should
 * be mandatory
 * @note [POSIX.1-2008 12.1 item 2] states that there are exceptions to the
 * above mentioned guideline to 'ensure continued operation of historical
 * applications'
 * @note Default behavior:\n
 * [-a arg]  //OK\n
 * [-a[arg]] //Not allowed
 */
#define ALLOW_OPTIONAL_ARGUMENTS true

/**
 * @brief Allow the presence of space before an optional option argument
 * @note This flag is only relevant if ALLOW_OPTIONAL_ARGUMENTS is defined as
 * true
 * @note [POSIX.1-2008 12.1 item 2.b] states that optional option arguments
 * should preceede the option directly, and that the argument should otherwise
 * be considered omitted.
 * @note Default behavior:\n
 * [-a arg] //Mandatory argument\n
 * -a42 //Option 'a' with argument 42\n
 * -b 42 //Option 'a' with argument 42\n
 * [-b[arg]] //Optional argument\n
 * -b42 //Option 'b' with argument 42\n
 * -b 42 //Option 'b' with no argument followed by operand '42'
 */
#define ALLOW_SPACE_BEFORE_OPTIONAL_ARGUMENT true

/**
 * @brief Allow options to follow after operands
 * @note [POSIX.1-2008 12.2 guideline 9] states that all options should precede
 * operands
 * @note Default behavior:
 * application -a ~/.somefile //OK\n
 * application ~/.somefile -a //Not allowed
 */
#define ALLOW_OPTIONS_FOLLOWING_OPERANDS true

//Diverge from 12.2 guideline 3 and 4
#define ALLOW_GNU_CONVENTION true

#include <vector>
#include <string>
#include <iostream>
using namespace std;

class Group;
class BaseOption;
class ArgumentManager
{
public:  
    static bool registerGroup(Group* group);
    static bool unregisterGroup(Group* group);

    static bool registerOption(BaseOption* option);
    static bool unregisterOption(BaseOption* option);

    static bool parse(int argc, char** argv);
    static void printUsage();

    static bool handleOption(string currentArgument, bool& operandEncoutered, bool& delimiterEncountered, int& argi, int argc, char** argv);

private:
    ArgumentManager();

    static Group* getGroup(string name);
    static BaseOption* getOption(char name);
    static BaseOption* getOption(string longName);

    static vector<Group*> g_groupList;
    static vector<BaseOption*> g_optionList;

    static void getApplicationName(string argv0);

    static string g_applicationName;
};

#endif // ARGUMENTMANAGER_H
