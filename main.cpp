#include <iostream>
using namespace std;

#include "group.h"
#include "option.h"


int main(int argc, char** argv)
{
    Group grp1("grp1");
    Group grp2("grp2");

    Option<> option1('a', "This does something");
    Option<> option2("optionb", "This does something else");
    Option<int> option3('c', "optionc", "some-arg1", "This has an optional arg", false, true, &grp1);
    Option<float> option4('d', "optiond", "some-prettylongargthisis", "This has an mandatory arg", true, false, &grp2);
    Option<bool> option5('e', "optione", "some-arg3", "This has an mandatory arg", true, false, &grp2);
    Option<string> option6('f', "optionf", "some-arg3", "This has an mandatory arg", true, false, &grp2);

    if(!ArgumentManager::parse(argc, argv))
    {
        ArgumentManager::printUsage();
        return 1;
    }

    ArgumentManager::printUsage();

    cout<<endl;
    if(option1.isPresent())
        cout<<"-a is present "<<option1.getCount()<<" times"<<endl;
    if(option2.isPresent())
        cout<<"-b is present"<<endl;
    if(option3.isPresent())
    {
        cout<<"-c is present with ";

        for(int i = 0; i < option3.getCount(); i++)
        {
            if(option3.hasArg(i))
                cout<<"arg: "<<option3.getArg(i)<<endl;
            else
                cout<<"no arg"<<endl;
        }
    }
    if(option4.isPresent())
    {
        cout<<"-d is present with ";
        if(option4.hasArg())
            cout<<"arg: "<<option4.getArg()<<endl;
        else
            cout<<"no arg"<<endl;
    }
    if(option5.isPresent())
    {
        cout<<"-e is present with ";
        if(option5.hasArg())
            cout<<"arg: "<<option5.getArg()<<endl;
        else
            cout<<"no arg"<<endl;
    }
    if(option6.isPresent())
    {
        cout<<"-f is present with ";
        if(option5.hasArg())
            cout<<"arg: "<<option6.getArg()<<endl;
        else
            cout<<"no arg"<<endl;
    }

    return 0;
}

