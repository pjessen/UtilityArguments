/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GROUP_H
#define GROUP_H

#include <string>
#include <vector>
#include <assert.h>
#include "argumentmanager.h"

using namespace std;

class BaseOption;
class Group
{
    friend class BaseOption;
public:
    Group(const string& name);

    const string& getName() const;
    const vector<BaseOption*> getOptions() const;

private:
    void addOption(BaseOption*);
    void removeOption(BaseOption*);

    string m_name;
    vector<BaseOption*> m_optionList;
};

#endif // GROUP_H
