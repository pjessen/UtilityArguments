/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPTION_H
#define OPTION_H

#include <assert.h>
#include <string>
#include <sstream>
#include <vector>
#include "argumentmanager.h"
#include "group.h"

using namespace std;

class DefaultType
{};

template<class T>
class OptionOccurence
{
public:
    OptionOccurence() : m_hasArgument(0)
    {}

    OptionOccurence(const T& arg) :
        m_hasArgument(true),
        m_argument(arg)
    {}

    bool hasArgument() const
    {
        return m_hasArgument;
    }

    const T& getArgument() const
    {
        return m_argument;
    }

private:
    bool m_hasArgument;
    T m_argument;
};

class BaseOption
{
public:
    enum class ArgumentType
    {
        NO_ARGUMENT,
        OPTIONAL_ARGUMENT,
        MANDATORY_ARGUMENT
    };

    enum class ParseArgumentResult
    {
        FAILED,
        NO_ARG,
        ARG
    };

    BaseOption(char name, const string& description, bool allowMultiple, Group* group);
    ~BaseOption();

    char getName() const;
    const string& getDescription() const;
    bool isPresent() const;
    int getCount() const;
    bool registerPresence();

    virtual string toString() const;
    virtual ArgumentType getArgumentType() const
    {
        return ArgumentType::NO_ARGUMENT;
    }

    virtual ParseArgumentResult parseArgument(const string&)
    {
        return ParseArgumentResult::NO_ARG;
    }

    virtual bool hasMandatoryArgument() const
    {
        return false;
    }

#if ALLOW_GNU_CONVENTION
    BaseOption(char name, string longName, string description, bool allowMultiple, Group* group);
    string getLongName() const;
#endif

private:
    char m_name;
    string m_longName;
    string m_description;


    bool m_allowMultiple;
    Group* m_group;
    int m_count;
};

template<typename T = DefaultType>
class Option : public BaseOption
{
public:
    Option(char name, string longName, string description, string argName, bool argMandatory = true, bool allowMultiple = false, Group* group = 0) :
        BaseOption(name, longName, description, allowMultiple, group),
        m_argName(argName)
    {
        if(argMandatory)
            m_argType = ArgumentType::MANDATORY_ARGUMENT;
        else
        {
#if ALLOW_OPTIONAL_ARGUMENTS
            m_argType = ArgumentType::OPTIONAL_ARGUMENT;
#else
            assert(false &&
                   "Optional arguments are not allowed. \
                    To enable, alter the ALLOW_OPTIONAL_ARGUMENTS flag");
#endif
        }
    }

    ~Option()
    {

    }

    bool hasMandatoryArgument() const override
    {
        return m_argType == ArgumentType::MANDATORY_ARGUMENT;
    }

    /**
     * @brief
     * @note In accordance to 12.1 item 2
     * @return
     */
    string toString() const override
    {
        string str = BaseOption::toString();


        if(m_argType == ArgumentType::OPTIONAL_ARGUMENT)
            str += "[";
        else
            str+= " ";
        str += m_argName;

        if(m_argType == ArgumentType::OPTIONAL_ARGUMENT)
            str += "]";

        return str;
    }

    ParseArgumentResult parseArgument(const string& argument) override
    {
        //Encapsule argument in stream
        stringstream stream(argument);

        //Attempt to read (and cast) argument
        T arg;
        bool r = stream>>arg;

        //Argument should not contain additional data
        if(stream.rdbuf()->in_avail() != 0)
        {
            if(m_argType == ArgumentType::OPTIONAL_ARGUMENT)
                return ParseArgumentResult::NO_ARG;
            else
                return ParseArgumentResult::FAILED;
        }

        if(r)
        {
            m_occurenceList.push_back(OptionOccurence<T>(arg));
            return ParseArgumentResult::ARG;
        }
        else if(m_argType == ArgumentType::OPTIONAL_ARGUMENT)
        {
            m_occurenceList.push_back(OptionOccurence<T>());
            return ParseArgumentResult::NO_ARG;
        }
        else
            return ParseArgumentResult::FAILED;
    }

    ArgumentType getArgumentType() const override
    {
        return m_argType;
    }

    bool hasArg(unsigned int occurence = 0) const
    {
        if(occurence < m_occurenceList.size())
            return m_occurenceList[occurence].hasArgument();

        return false;
    }

    const T& getArg(unsigned int occurence = 0)
    {
        if(occurence < m_occurenceList.size())
            return m_occurenceList[occurence].getArgument();
    }

private:
    string m_argName;
    ArgumentType m_argType;

    vector<OptionOccurence<T>> m_occurenceList;
};


template<>
class Option<DefaultType> : public BaseOption
{
public:
    Option(char name, string description, bool allowMultiple = false, Group* group = 0) :
        BaseOption(name, description, allowMultiple, group)
    {
    }

#if ALLOW_GNU_CONVENTION
    Option(string longName, string description, bool allowMultiple = false, Group* group = 0) :
        BaseOption(0, longName, description, allowMultiple, group)
    {
    }

    Option(char name, string longName, string description, bool allowMultiple = false, Group* group = 0) :
        BaseOption(name, longName, description, allowMultiple, group)
    {
    }
#endif
};

#endif // OPTION_H
