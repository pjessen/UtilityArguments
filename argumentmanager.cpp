/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "argumentmanager.h"
#include "group.h"
#include "option.h"

vector<Group*> ArgumentManager::g_groupList;
vector<BaseOption*> ArgumentManager::g_optionList;
string ArgumentManager::g_applicationName;

ArgumentManager::ArgumentManager()
{

}

bool ArgumentManager::registerGroup(Group *group)
{
    if(getGroup(group->getName()))
        return false;

    g_groupList.push_back(group);
    return true;
}

bool ArgumentManager::unregisterGroup(Group *group)
{
    for(unsigned int i = 0; i < g_groupList.size(); i++)
    {
        if(g_groupList[i] == group)
        {
            g_groupList.erase(g_groupList.begin() + i);
            return true;
        }
    }

    return false;
}

bool ArgumentManager::registerOption(BaseOption *option)
{
    if(getOption(option->getName()))
        return false;

#if ALLOW_GNU_CONVENTION
    if(getOption(option->getLongName()))
        return false;
#endif

    g_optionList.push_back(option);
    return true;
}

bool ArgumentManager::unregisterOption(BaseOption *option)
{
    for(unsigned int i = 0; i < g_optionList.size(); i++)
    {
        if(g_optionList[i] == option)
        {
            g_optionList.erase(g_optionList.begin() + i);
            return true;
        }
    }

    return false;
}

Group* ArgumentManager::getGroup(string name)
{
    for(Group* g : g_groupList)
    {
        if(g->getName() == name)
            return g;
    }

    return 0;
}

BaseOption *ArgumentManager::getOption(char name)
{
    for(BaseOption* g : g_optionList)
    {
        if(g->getName() == name)
            return g;
    }

    return 0;
}

BaseOption *ArgumentManager::getOption(string longName)
{
    for(BaseOption* g : g_optionList)
    {
        if(g->getLongName() == longName)
            return g;
    }

    return 0;
}


bool ArgumentManager::handleOption(string currentArgument, bool& operandEncoutered, bool& delimiterEncountered, int& argi, int argc, char** argv)
{
    //Is the argument an option? (eg. starts with '-')
    if(currentArgument[0] == '-' && !delimiterEncountered)
    {
        //Remove '-' character
        currentArgument.erase(0, 1);

        //'-' should be followed by an option
        if(!currentArgument.length())
        {
            cout<<"Expected an option after '-'"<<endl;
            return false;
        }

        //Option pointer
        BaseOption* option;

        //Double dash
        if(currentArgument[0] == '-')
        {
#if ALLOW_GNU_CONVENTION
            currentArgument = currentArgument.substr(1);
            if(currentArgument.length())
                option = getOption(currentArgument);
            else
                delimiterEncountered = true;
#else
            delimiterEncountered = true;
            return true;
#endif
        }

#if ALLOW_OPTIONS_FOLLOWING_OPERANDS == false
        //Options are not allowed after operands
        if(operandEncoutered)
        {
            cout<<"Options are not allowed after operands"<<endl;
            return false;
        }
#endif

        //Loop through characters
        for(unsigned int i = 0; i < currentArgument.length(); i++)
        {
            //Get current option
            char currentOption = currentArgument[i];

            //Determine the remaining argument
            string remaingArg;
            if(i+1 < currentArgument.length())
                remaingArg = currentArgument.substr(i+1);

            //Look up the option
            BaseOption* opt = getOption(currentOption);
            if(!opt)
            {
                cout<<"Invalid argument: -"<<currentOption<<endl<<endl;
                return false;
            }

            //Register its presence
            if(!opt->registerPresence())
            {
                cout<<"Option '-"<<opt->getName()<<" is only allowed once"<<endl;
                return false;
            }

            //There is still more data in this argument
            if(remaingArg.length())
            {
                if(!opt->hasMandatoryArgument())
                {
                    //Argument is not mandatory, check if next char is another
                    //option
                    if(getOption(remaingArg[0]))
                        continue;
                }

                BaseOption::ParseArgumentResult result = opt->parseArgument(remaingArg);

                if(result == BaseOption::ParseArgumentResult::FAILED)
                {
                    cout<<"Missing argument to: "<<opt->getName()<<endl;
                    return false;
                }
                else if(result == BaseOption::ParseArgumentResult::NO_ARG)
                {
                }
                else if(result == BaseOption::ParseArgumentResult::ARG)
                {
                    break;
                }
            }
            //No more data in this argument
            else
            {
                if(argi + 1 == argc)
                {
                    if(opt->hasMandatoryArgument())
                    {
                        cout<<"Missing argument to: "<<opt->getName()<<endl;
                        return false;
                    }

                    break;
                }

#if ALLOW_SPACE_BEFORE_OPTIONAL_ARGUMENT

                BaseOption::ParseArgumentResult result = opt->parseArgument(argv[argi + 1]);

                if(result == BaseOption::ParseArgumentResult::FAILED)
                {
                    cout<<"Missing argument to: "<<opt->getName()<<endl;
                    return false;
                }
                else if(result == BaseOption::ParseArgumentResult::NO_ARG)
                {
                }
                else if(result == BaseOption::ParseArgumentResult::ARG)
                {
                    argi++;
                    break;
                }
#else
                if(opt->hasMandatoryArgument())
                {
                    BaseOption::ParseArgumentResult result = opt->parseArgument(argv[argi + 1]);

                    if(result == BaseOption::FAILED)
                    {
                        cout<<"Missing argument to: "<<opt->getName()<<endl;
                        return false;
                    }
                    else if(result == BaseOption::ARG)
                    {
                        argi++;
                        break;
                    }
                }
#endif
            }
        }
    }
    else
    {
        //operandEncoutered = true;
        cout<<"Operands: "<<currentArgument<<endl;
    }
    return true;
}

bool ArgumentManager::parse(int argc, char** argv)
{
    //Obtain application name from argv[0]
    getApplicationName(argv[0]);

    //Update argc and argv to point at following arguments
    argc--;
    argv++;

    bool operandEncountered = false;

    //Flag if '--' has been encoutered
    bool delimiterEncountered = false;

    //Iterate the arguments
    for(int argi = 0; argi < argc; argi++)
    {
        if(!handleOption(argv[argi], operandEncountered, delimiterEncountered, argi, argc, argv))
            return false;
    }

    return true;
}

void ArgumentManager::printUsage()
{
    cout<<"Usage:"<<endl;
    cout<<"  "<<g_applicationName<<endl<<endl;

    for(Group* g : g_groupList)
    {
        //Ignore empty groups
        if(!g->getOptions().size())
            continue;

        cout<<g->getName()<<":"<<endl;

        //Find the longest option
        int longestOption = 0;
        for(BaseOption* o : g->getOptions())
        {
            if(o->toString().length() > longestOption)
                longestOption = o->toString().length();
        }

        for(BaseOption* o : g->getOptions())
        {
            string s = o->toString();
            int diff = longestOption - s.length();

            cout<<"  "<<o->toString();

            for(int i = 0; i < diff / 6; i++)
                cout<<"\t";

            cout<<"\t"<<o->getDescription()<<endl;
        }

        cout<<endl;
    }
}

void ArgumentManager::getApplicationName(string argv0)
{
    for(int i = argv0.length() - 1; i >= 0; i--)
    {
        if(argv0[i] == '/' || argv0[i] == '\\')
        {
            g_applicationName = argv0.substr(i + 1);
            break;
        }
    }
}
