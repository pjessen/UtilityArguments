/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "group.h"
#include "option.h"

Group::Group(const string &name) :
    m_name(name)
{
    assert(ArgumentManager::registerGroup(this) &&
          "Group name already exists");
}

const string& Group::getName() const
{
    return m_name;
}

const vector<BaseOption *> Group::getOptions() const
{
    return m_optionList;
}

void Group::addOption(BaseOption* option)
{
    m_optionList.push_back(option);
}

void Group::removeOption(BaseOption* option)
{
    for(unsigned int i = 0; i < m_optionList.size(); i++)
    {
        if(m_optionList[i] == option)
        {
            m_optionList.erase(m_optionList.begin() + i);
            break;
        }
    }
}
