/**
 *  UtilityArguments - Library that enables POSIX compliant utility arguments
 *  Copyright (C) 2015 Patrick Jessen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "option.h"

Group defaultGroup("Options");

BaseOption::BaseOption(char name, string longName, string description, bool allowMultiple, Group* group) :
    m_name(name),
    m_longName(longName),
    m_description(description),
    m_allowMultiple(allowMultiple),
    m_group(group),
    m_count(0)
{
    if(!m_group)
        m_group = &defaultGroup;

    m_group->addOption(this);

    assert(ArgumentManager::registerOption(this) &&
           "Option name already exists");
}

BaseOption::BaseOption(char name, const string &description, bool allowMultiple, Group *group) :
    m_name(name),
    m_description(description),
    m_allowMultiple(allowMultiple),
    m_group(group),
    m_count(0)
{
    if(!m_group)
        m_group = &defaultGroup;

    m_group->addOption(this);

    assert(ArgumentManager::registerOption(this) &&
           "Option name already exists");
}

BaseOption::~BaseOption()
{
    if(m_group)
        m_group->removeOption(this);

    ArgumentManager::unregisterOption(this);
}

char BaseOption::getName() const
{
    return m_name;
}

string BaseOption::getLongName() const
{
    return m_longName;
}

const string& BaseOption::getDescription() const
{
    return m_description;
}

bool BaseOption::isPresent() const
{
    return (bool)m_count;
}

bool BaseOption::registerPresence()
{
    if(!m_allowMultiple && m_count)
        return false;

    m_count++;

    return true;
}

string BaseOption::toString() const
{
    string str;

    if(m_name != 0)
    {
        str = "-";
        str += m_name;
    }

#if ALLOW_GNU_CONVENTION
    if(m_longName.length())
    {
        if(m_name != 0)
            str += ", ";

        str += "--";
        str += m_longName;
    }
#endif

    return str;
}

int BaseOption::getCount() const
{
    return m_count;
}
