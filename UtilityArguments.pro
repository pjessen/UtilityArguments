TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    argumentmanager.cpp \
    group.cpp \
    option.cpp

include(deployment.pri)
qtcAddDeployment()

DISTFILES += \
    README \
    licenseTemplate \
    COPYING

HEADERS += \
    option.h \
    argumentmanager.h \
    group.h

